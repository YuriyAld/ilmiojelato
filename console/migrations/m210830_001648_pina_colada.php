<?php

use yii\db\Migration;

/**
 * Class m210830_001648_pina_colada
 */
class m210830_001648_pina_colada extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('ice', [
            'name' => 'Пинья колада',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('ice', ['name' => 'Пломбир']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210830_001648_pina_colada cannot be reverted.\n";

        return false;
    }
    */
}
