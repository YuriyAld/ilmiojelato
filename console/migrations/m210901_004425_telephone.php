<?php

use yii\db\Migration;

/**
 * Class m210901_004425_telephone
 */
class m210901_004425_telephone extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%telephone}}', [
            'id' => $this->primaryKey()->notNull(),
            'telephone' => $this->decimal(),
            'address' => $this->text(), 
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%telephone}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210901_004425_telephone cannot be reverted.\n";

        return false;
    }
    */
}
