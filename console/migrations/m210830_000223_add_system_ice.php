<?php

use yii\db\Migration;

/**
 * Class m210830_000223_add_system_ice
 */
class m210830_000223_add_system_ice extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%goods}}', [
            'id' => 1,
            'name_goods' => 'мороженное'
        ]); 
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%goods}}', ['id' => 1]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210830_000223_add_system_ice cannot be reverted.\n";

        return false;
    }
    */
}
