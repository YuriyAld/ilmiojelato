<?php

use yii\db\Migration;

/**
 * Class m210830_001320_add_ice_plombir
 */
class m210830_001320_add_ice_plombir extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('ice', [
            'name' => 'Пломбир',
            'desc' => 'Пломбир он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('ice', ['name' => 'Пломбир']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210830_001320_add_ice_plombir cannot be reverted.\n";

        return false;
    }
    */
}
