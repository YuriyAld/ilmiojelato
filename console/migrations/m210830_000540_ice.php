<?php

use yii\db\Migration;

/**
 * Class m210830_000540_ice
 */
class m210830_000540_ice extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%ice}}', [
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string()->notNull(),
            'desc' => $this->text(),
            'amount' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'goods_id' => $this->integer() 
        ]);
        $this->addForeignKey(
            '{{%fk-ice-goods_id}}',
            '{{%ice}}',
            'goods_id',
            '{{%goods}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ice}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210815_191012_ice cannot be reverted.\n";

        return false;
    }
    */
}
