<?php

use yii\db\Migration;

/**
 * Class m210830_000026_system_ice
 */
class m210830_000026_system_ice extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%goods}}', [
            'id' => $this->primaryKey(),
            'name_goods' => $this->string()->notNull(), 
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%goods}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210830_000026_system_ice cannot be reverted.\n";

        return false;
    }
    */
}
