<?php

use yii\db\Migration;

/**
 * Class m210831_222904_almoust_positions
 */
class m210831_222904_almoust_positions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('ice', [
            'name' => 'Арбуз',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
        $this->insert('ice', [
            'name' => 'Баблгам',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
        $this->insert('ice', [
            'name' => 'Банан',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
        $this->insert('ice', [
            'name' => 'Баслийское Манго',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
        $this->insert('ice', [
            'name' => 'Бельгийский Шоколад',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
        $this->insert('ice', [
            'name' => 'Голубое Небо',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);

        $this->insert('ice', [
            'name' => 'Фисташки',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
        $this->insert('ice', [
            'name' => 'Французский Крем-Брюле',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
        $this->insert('ice', [
            'name' => 'Зеленая Мята',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
        $this->insert('ice', [
            'name' => 'Испанская Клубника',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
        $this->insert('ice', [
            'name' => 'Киви',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
        $this->insert('ice', [
            'name' => 'Лимон',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
        $this->insert('ice', [
            'name' => 'Мараканский Апельсин',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
        $this->insert('ice', [
            'name' => '',
            'desc' => 'Пинья колада он и в африке пломбир',
            'amount' => 10,
            'price' => 1000,
            'goods_id' => 1,
        ]);
                                        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210831_222904_almoust_positions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
    echo "m210831_222904_almoust_positions cannot be reverted.\n";

    return false;
    }
    */
}
