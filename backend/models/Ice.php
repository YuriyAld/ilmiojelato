<?php

namespace backend\models;

use Yii;
use \backend\models\Goods;

/**
 * This is the model class for table "{{%ice}}".
 * @property int $id  
 * @property string $name  
 * @property string|null $desc  
 *   @property int $amount
 *   @property int $price
 *   @property int|null $goods_id
 *   
 *   @property Goods $goods
 */
    class Ice extends \yii\db\ActiveRecord
    {
        /**
         *
         *   {@inheritdoc} 
         *
         */
        public static function tableName()
        {
            return '{{%ice}}';
        }

        /**
         *
         *   {@inheritdoc} 
         *
         */
        public function rules()
        {
            return [
                [['name', 'amount', 'price'], 'required'],
                [['desc'], 'string'],
                [['amount', 'price', 'goods_id'], 'integer'],
                [['name'], 'string', 'max' => 255],
                [['goods_id'], 'exist', 'skipOnError' => true, 'targetClass' => Goods::className(), 'targetAttribute' => ['goods_id' => 'id']],
            ];
        }

        /**
         *
         *   {@inheritdoc} 
         *
         */
        public function attributeLabels()
        {
            return [
                'id' => 'ID',
                'name' => 'Имя',
                'desc' => 'Описание',
                'amount' => 'Количество',
                'price' => 'Цена',
                'goods_id' => 'Категория Товара',
            ];
        }

        /**
         *
         *   gets query for [[Goods]]
         * @return \yii\db\ActiveQuery
         */
        public function getGoods()
        {
            return $this->hasOne(Goods::className(), ['id' => 'goods_id']);
        }

        /**
         *
         *   если количество меньше нуля то количество равно нулю
         *   @return $this->amount 
         */
        public function isAmount()
        {
            return $this->amount < 0 ? $this->amount = 0 : $this->amount;  
        }
    }
