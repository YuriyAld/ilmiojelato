<?php

namespace backend\models;

use Yii;

/**
 *
 *   this is the model class for table "{{%goods}}.
 * @property int $id
 * @property string $name_goods
 *
 * @property Ice[] $ices
 *
 */
    class Goods extends \yii\db\ActiveRecord
    {
        /**
         *
         *   {@inheritdoc} 
         *
         */
        public static function tableName()
        {
            return '{{%goods}}';
        }

        /**
         *
         *   {@inheritdoc} 
         *
         */
        public function rules()
        {
            return [
                [['name_goods'], 'required'],
                [['name_goods'], 'string', 'max' => 255],
            ];
        }

        /**
         *
         *   {@inheritdoc} 
         *
         */
        public function attributeLabels()
        {
            return [
                'id' => 'ID',
                'name_goods' => 'Имя Категории Товаров',
            ];
        }

        /**
         * Gets query for [[Ices]]
         *  @return \yii\db\ActiveQuery
         *
         */
        public function getIce()
        {
            return $this->hasMany(Ice::className(), ['goods_id' => 'id']);   
        }
    }


