<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Ice;

/**
 *
 *   searcIce represetns the model behind the search form 'ackend\models\ICe'
 *
 */
    class SearchIce extends Ice
    {
        public function rules()
        {
            return [
                [['id', 'amount', 'price', 'goods_id'], 'integer'],
                [['name', 'desc'], 'safe'],
            ];
        }


        /**
         *
         *   будущие сценарии для валидации если понадобиться
         *
         */
        public function scenarios()
        {
            // bypass scenarios() implementation in the parent class
            return Model::scenarios();
        }

        /**
         * creates data provider instance with sarch query applied
         * @param array $params
         *  @return ActiveDataProvider  
         *
         */
        public function search($params)
        {
            $query = Ice::find();

            // add conditions that should always apply here

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);

            $this->load($params);

            if (!$this->validate() )
            {
                // uncommetn the following line if you do not want to return any records when validation fails
                // $query -> where('0=1');
                return $dataProvider;
            }

            // grid filtering conditions
            $query -> andFilterWhere([
                'id' => $this->id,
                'amount' => $this->amount,
                'price' => $this->price,
                'goods_id' => $this->goods_id, 
            ]);

            $query ->andFilterWhere(['like', 'name', $this->name]) ->andFilterWhere(['like', 'desc', $this->desc]);

            return $dataProvider;
        }
    }
