<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "telephone".
 *
 * @property int $id
 * @property int $telephone
 * @property string $address
 */
class Telephone extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telephone';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telephone'], 'required'],
            [['address'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

            'telephone' => 'Telephone',
            'address' => 'Address',
        ];
    }
}
