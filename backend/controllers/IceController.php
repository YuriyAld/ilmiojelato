<?php

namespace backend\controllers;

use Yii;
use backend\models\Goods;
use backend\models\Ice;
use backend\models\SearchIce;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * IceController implements the CRUD action for Ice model
 */
    class IceController extends \yii\web\Controller 
    {
        public function behaviors()
        {
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'actions' => ['login', 'error'],
                            'allow' => true,
                        ],
                        [
                            'actions' => ['logout', 'index', 'update', 'view', 'test'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'logout' => ['post'],
                    ],
                ],
            ];
        }


        public function actions()
        {
            return [
                'error' => [
                    'class' => 'yii\web\ErrorAction',
                ],
            ];
        }
     

        /**
         *
         *   lists all Ice models
         *  @return mixed
         */
        public function actionIndex()
        {
            $searchModel = new SearchIce();
            $dataProvider = $searchModel -> search($this->request -> queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        
        /**
         * updates an existing Ice model.
         * if update is successful, the browser will be redirected to the 'view' page
         * @param int $id ID
         * @return mixed
         * @throws NOtFoundHttpException if the model cannot be found
         */
        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            if ($this->request -> isPost && $model -> load($this->request -> post()) && $model -> save() )
            {
                return $this->redirect(['view', 'id' => $model -> id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);            
        }

        /**
         * displays a single Ice model
         *   @param int $id ID  
         *   @return mixed
         *   @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        /**
         * finds the Ice model based on its primary key value.
         *   if the model is ont found a 404 http exception will be thrown
         *   @param int $id iD
         *   @return Ice the loaded model
         *   @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
            if (($model = Ice::findOne($id)) !== null)
            {
                return $model;
            }

            throw new NOtFoundHttpException('The requested page does not exist.');
        }

    }
