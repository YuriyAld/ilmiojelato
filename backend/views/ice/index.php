<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchIce */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мороженное';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="ice-index">
    <h1><?= Html::encode($this->title)  ?></h1>

    <?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
	    ['class' => 'yii\grid\SerialColumn'],
	    'name',
	    'amount',
	    'price',
	    'desc:ntext',

	    ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
	],
    ]);  ?>

</div>
