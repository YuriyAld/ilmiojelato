<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Ice */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Мороженное', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ice-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить в '. $model -> name . ' мороженное', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'amount',
            'price',
            'id',
            'name',
            'desc:ntext',
            'goods_id',
        ],
    ]) ?>

</div>
