<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Ice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ice-form">

    <?php $form = ActiveForm::begin([
	'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    
    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'desc')->textarea(['rows' => 6]) ?>

    <?php ActiveForm::end(); ?>

</div>
