<?php

namespace frontend\components;

use yii\base\BaseObject;


    class Elf extends BaseObject
    {
        public $name;
        public $position;

        public function setName($name)
        {
            $this->name = $name; 
        }

        public function getName()
        {
            return $this->name;  
        }
    }
