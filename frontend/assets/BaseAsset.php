<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
*
*   child frontend application assset bundle
*
*/
class BaseAsset extends AssetBundle
{
public $basePath = '@webroot';
public $baseUrl = '@web';
public $css = [
	       "css/style.css",
               "https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap",
	       "css/all.min.css",
	       "css/owl.carousel.min.css",
	       "css/lightbox.min.css",
	       ];
public $js = [
	      "js/jquery-3.4.1.min.js",
	      "js/bootstrap.bundle.min.js",
	      "js/easing.min.js",
	      "js/waypoints.min.js",
	      "js/owl.carousel.min.js",
	      "js/isotope.pkgd.min.js",
	      "js/lightbox.min.js",
	      "js/jqBootstrapValidation.min.js",
	      "js/contact.js",
	      "js/main.js",
              
	      ];

}
