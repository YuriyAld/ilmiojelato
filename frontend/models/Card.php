<?php

namespace frontend\models;

use Yii;

/**
 *
 *   Здесь модель Корзины которая через контроллер IceController будет осуществлять выбор количества мороженного
 *   и отправку этого выбора в Сессию а также запись туда идентификатора при чем их может быть больше одного
 *   их количество должно быть ограниченно. Например 10
 *  
 */
    class Card extends yii\base\Model
    {
        public $choice_one = 'choice one';
        public $choice_two = 'chocie two';
        public $choice_three = 'chocie three';
        public $choice_four = 'choice_four';
        public $choice = 'choice';

        public function rules()
        {
            return [
                [['choice_one', 'choice_two', 'choice_three', 'choice_four', 'choice'], 'required'],
            ];
        }

    }
