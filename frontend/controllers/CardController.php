<?php

namespace frontend\controllers;

use Yii;
use backend\models\Goods;
use backend\models\Ice;
use frontend\models\Card;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 *
 *   Контроллер для Корзины
 *   будет содержать функции - выбрать одно мороженное 
 *  два мороженного
 *  три мороженного
 *  четыре мороженного
 *  или выбор пользователя
 *  еще нужно осуществить удаление из Сессии - очищение корзины
 *  в сессию нужно записать id при чем их должно быть максимум 10
 *  и остальные параметры - price, name
 */
class CardController extends \yii\web\Controller
{
        /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['one', 'two', 'three', 'four'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     *
     *   происходит запись сессии и отрисовка основной страницы магазина которая скопирована от index.php site controller
     *   принимается $id и в зависимости от выбора
     *   один два три или четыре
     *   в сессиию записывается этот вариант
     *   
     *   
     */
    public function actionOne($id)
    {
        /**
         *
         *   объявляю сессию
         *
         */
        $session = Yii::$app -> session;
        /**
         *
         *   устанавливаю параметр выбора и идентификатор
         *
         */
        $session -> set('choice', '1');
        $session -> set('id', $id);
        /**
         *
         *   имя продукта это вызов из БД зная ид
         *
         */
        $ice = Ice::findOne($id);
        $name = $ice -> name;

        return $this-> render('index', [
            'session' => $session,
            'name' => $name,
        ]);
    }

    public function actionTwo($id)
    {
        $session = Yii::$app -> session;
        $session -> set('choice', '2');
        $session -> set('id', $id);

        $ice = Ice::findOne($id);
        $name = $ice -> name;

        return $this->render('index', [
            'session' => $session,
            'name' => $name,
        ]);
    }

    public function actionThree($id)
    {
        $session = Yii::$app -> session;
        $session -> set('choice', '3');
        $session -> set('id', $id);

        $ice = Ice::findOne($id);
        $name = $ice -> name;

        return $this->render('index', [
            'session' => $session,
            'name' => $name,
        ]);
    }

    public function actionFour($id)
    {
        $session = Yii::$app -> session;
        $session -> set('choice', '4');
        $session -> set('id', $id);

        $ice = Ice::findOne($id);
        $name = $ice -> name;
        
        return $this->render('index', [
            'session' => $session,
            'name' => $name,
        ]);
    }

    /**
     *
     *   здесь осуществляется покупка необходим механизм и логика удаления товара из базы данных количетсва из сессии
     *
     */
    public function actionBuy($id)
    {
        $session = Yii::$app -> session;
        /**
         *
         *   количество из сессии
         *
         */        
        $count_from_session = $session -> get('choice');
        
        $ice = Ice::findOne($id);
        $name = $ice -> name;

        /**
         *
         *   осуществляю удаление стольких товара сколько выбрал пользователь
         *
         */
        $ice -> amount = $ice -> amount - $count_from_session;
        $ice -> isAmount();
        $ice -> save();

        /**
         *
         *   отправляю письмо пока пробное самому себе
         *
         */
        Yii::$app->mailer->compose()
                         ->setFrom('huravltampl@gmail.com')
                         ->setTo('huravltampl@gmail.com')
                         ->setSubject('jacke queen king dolly')
                         ->setTextBody('jacke queen king dolly')
                         ->setHtmlBody('<b>dolores</b>')
                         ->send();

        /**
         *
         *   отрисовка где пишется что покупка осуществилась и далее магазин
         *
         */
        return $this->render('buy',[
            'id' => $id,
            'name' => $name,
        ]); 

    }

}
