<?php

namespace frontend\controllers;

use Yii;
use backend\models\Goods;
use backend\models\Ice;
use frontend\models\Card;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 *
 *   icecontroller from frontend side
 *
 */
    class IceController extends \yii\web\Controller
    {
        public function behaviors()
        {
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'actions' => ['login', 'error'],
                            'allow' => true,
                        ],
                        [
                            'actions' => ['logout', 'store', 'choice', 'card'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'logout' => ['post'],
                    ],
                ],
            ];
        }

        public function actions()
        {
            return [
                'error' => [
                    'class' => 'yii\web\ErrorAction',
                ],
            ];
        }

        /**
         *
         *   сдесь нужно вывести все мороженное в красивом виде
         *
         */
        public function actionStore()
        {
            $altay = Ice::findOne(1);
            $ananas = Ice::findOne(2);
            $arbuz = Ice::findOne(3);
            $bablgam = Ice::findOne(4);
            $banab = Ice::findOne(5);
            $basliyskoeMango = Ice::findOne(6);
            $belgiyskiyChocolate = Ice::findOne(7);
            $blueNebo = Ice::findOne(8);
            $fistashki = Ice::findOne(9);
            $franceKremBrule = Ice::findOne(10);
            $greenMatya = Ice::findOne(11);
            $ispanskayaKlubnika = Ice::findOne(12);
            $kivi = Ice::findOne(13);
            $limon = Ice::findOne(14);
            $marokanskiyApelsin = Ice::findOne(15);

                
            return $this->render('store', [
                'altay' => $altay,
                'ananas' => $ananas,
                'arbuz' => $arbuz,
                'bablgam' => $bablgam,
                'banab' => $banab,
                'basliyskoeMango' => $basliyskoeMango,
                'belgiyskiyChocolate' => $belgiyskiyChocolate,
                'blueNebo' => $blueNebo,
                'fistashki' => $fistashki,
                'franceKremBrule' => $franceKremBrule,
                'greenMatya' => $greenMatya,
                'ispanskayaKlubnika' => $ispanskayaKlubnika,
                'kivi' => $kivi,
                'limon' => $limon,
                'marokanskiyApelsin' => $marokanskiyApelsin,                
            ]);
        }

        /**
         *
         *   выбор мороженного принимает идентификатор товара из Базы Данных
         *   отправляет в отдел вью для выбора нужно выбрать количество мороженного
         */
        public function actionChoice($id)
        {
            $ices = Ice::find()->all();
            $ice = Ice::findOne($id);
            $card = Yii::$app -> card;

            /**
             *
             *   скриптик который принимает $id и в зависимости от того какой это номер (они от 1 до 15 могут быть)
             *   переменной $img присваивается название jpg file - и затем вызывается это изображение с хранилища изображений
             */
            switch($id)
            {
            case 1:
                $img = 'oblepiha.jpg';
                break;
            case 2:
                $img = 'chance_ananas.jpg';
                break;
            case 3:
                $img = 'choice_arbuz.jpg';
                break;
            case 4:
                $img = 'change_bablgam.jpg';
                break;
            case 5:
                $img = 'change_banan.jpg';
                break;
            case 6:
                $img = 'change_basliyskoeMango.jpg';
                break;
            case 7:
                $img = 'change_belgiyskiyChocolate.jpg';
                break;
            case 8:
                $img = 'change_blueNebo.jpg';
                break;
            case 9:
                $img = 'change_fistashki.jpg';
                break;
            case 10:
                $img = 'change_franceKremBrule.jpg';
                break;
            case 11:
                $img = 'change_greenMatya.jpg';
                break;
            case 12:
                $img = 'change_ispanskayaKlubnika.jpg';
                break;
            case 13:
                $img = 'kivi.jpg';
                break;
            case 14:
                $img = 'limon.jpg';
                break;
            case 15:
                $img = 'marokanskiyApelsin.jpg';
                break;
            default:
                echo 'default';
            }
            
            return $this->render('choice', [
                'id' => $id,
                'ice' => $ice,
                'card' => $card,
                'ices' => $ices,
                'img' => $img,
            ]);

        }
    }
