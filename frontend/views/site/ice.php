<?php
use yii\helpers\Html;
/* @var $this yii\web\View */


$this->title = 'ice store';

?>

<!-- Products Start -->
<div class="container-fluid py-5">
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <h1 class="section-title position-relative text-center mb-5">Best Prices We Offer For Ice Cream Lovers</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/altayskayOblepiha.jpg" style="object-fit: cover;"> 
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/ananas.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/arbuz.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/bablgam.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/banab.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/basliyskoeMango.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/belgiyskiyChocolate.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
	    </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/blueNebo.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
            </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/belgiyskiyChocolate.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
	    </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/fistashki.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
	    </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/franceKremBrule.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
	    </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/greenMatya.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
	    </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/ispanskayaKlubnika.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
	    </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/kivi.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
	    </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/limon.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
                    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
	    </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
		    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0">$99</h4>
		    </div>
		    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="user_img/marokanskiyApelsin.jpg" style="object-fit: cover;">
		    </div>
		    <h5 class="font-weight-bold mb-4">Vanilla Ice Cream</h5>
		    <a href="" class="btn btn-sm btn-secondary">Order Now</a>
                </div>
	    </div>
	    <div class="col-12 text-center">
                <a href="" class="btn btn-primary py-3 px-5">Load More</a>
	    </div>
        </div>
    </div>
</div>
<!-- Products End -->

