<?php
use yii\bootstrap4\Html;
use yii\widgets\ActiveForm;

$this->title = 'Выбор';

/**
 *
 * Скриптик для сокращения рутинной работы - входные данные - id
 *
 */

echo Html::a(
    Html::tag('h1', 'ВЕРНУТЬСЯ В МАГАЗИН',
	      ['class' => 'm-0 display-4 text-primary']),
    ['ice/store']);
?> 
<!-- Promotion Start -->
<div class="container-fluid my-5 py-5 px-0">
    <?= Html::img('@web/user_img/'.$img)  ?>
    <div class="row bg-primary m-0">
        <div class="col-md-6 py-5 py-md-0 px-0">
            <div class="h-100 d-flex flex-column align-items-center justify-content-center text-center p-5">
                <div class="d-flex align-items-center justify-content-center bg-white rounded-circle mb-4" style="width: 100px; height: 100px;">
                    <h3 class="font-weight-bold text-secondary mb-0"><?= $ice -> price  ?><br/><?= ' тг'  ?></h3>
                </div>
                <h3 class="font-weight-bold text-white mt-3 mb-4"><?= $ice -> name  ?></h3>
		<h3 class="font-weight-bold text-white mt-3 mb-4"> Имеется в наличии <?= $ice -> amount . ' упаковок ' ?></h3>
                <p class="text-white mb-4"><?= $ice -> desc  ?></p>

		<h4 class="font-weight-medium text-white mt-4 mb-4">Сколько Мороженного Вы хотите?</h4> 
		<?= Html::a(
		    Html::tag('h1', '1',
			      ['class' => 'btn btn-danger py-3 px-4 mt-2']),
		    ['card/one', 'id' => $id]
		)  ?>
		<?= Html::a(
		    Html::tag('h1', '2',
			      ['class' => 'btn btn-danger py-3 px-4 mt-2']),
		    ['card/two', 'id' => $id]
		)  ?>
		<?= Html::a(
		    Html::tag('h1', '3',
			      ['class' => 'btn btn-danger py-3 px-4 mt-2']),
		    ['card/three', 'id' => $id]
		)  ?>
		<?= Html::a(
		    Html::tag('h1', '4',
			      ['class' => 'btn btn-danger py-3 px-4 mt-2']),
		    ['card/four', 'id' => $id]
		)  ?>
            </div>
        </div>
    </div>
</div>
<!-- Promotion End -->
<?php
echo Html::a(
    Html::tag('h1', 'ВЕРНУТЬСЯ В МАГАЗИН',
	      ['class' => 'm-0 display-4 text-primary']),
    ['ice/store']);
?>
