<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'магазин мороженного';

?>

<!-- Products Start -->
<div class="container-fluid py-5">
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <h1 class="section-title position-relative text-center mb-5">Лучшие Цены Итальянского Мороженного!!!</h1>
            </div>
        </div>
	
        <div class="row">
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $altay -> price  ?> <br/> <?= ' тг'  ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/altayskayOblepiha.jpg" style="object-fit: cover;"> 
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $altay -> name  ?></h5>
		    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $altay -> id]
		    )  ?>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $ananas -> price ?><br/><?= ' тг'   ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/ananas.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $ananas -> name  ?></h5>
		    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $ananas -> id]
		    )  ?>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $arbuz -> price ?><br/><?= ' тг'   ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/arbuz.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $arbuz -> name  ?></h5>
		    		    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $arbuz -> id]
		    )  ?>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $bablgam -> price ?><br/><?= ' тг'   ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/bablgam.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $bablgam -> name  ?></h5>
		    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $bablgam -> id]
		    )  ?>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $banab -> price ?><br/><?= ' тг'   ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/banab.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $banab -> name  ?></h5>
		    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $banab -> id]
		    )  ?>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $basliyskoeMango -> price ?><br/><?= ' тг'   ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/basliyskoeMango.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $basliyskoeMango -> name  ?></h5>
		    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $basliyskoeMango -> id]
		    )  ?>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $belgiyskiyChocolate -> price ?><br/><?= ' тг'   ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/belgiyskiyChocolate.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $belgiyskiyChocolate -> name  ?></h5>
		    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $belgiyskiyChocolate -> id]
		    )  ?>
                </div>
	    </div>
            <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $blueNebo -> price ?><br/><?= ' тг'   ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/blueNebo.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $blueNebo -> name  ?></h5>
		    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $blueNebo -> id]
		    )  ?>
                </div>
            </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $fistashki -> price ?><br/><?= ' тг'   ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/fistashki.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $fistashki -> name  ?></h5>
		    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $fistashki -> id]
		    )  ?>
                </div>
	    </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $franceKremBrule -> price ?><br/><?= ' тг'   ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/franceKremBrule.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $franceKremBrule -> name  ?></h5>
                    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $franceKremBrule -> id]
		    )  ?>
                </div>
	    </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $greenMatya -> price ?><br/><?= ' тг'   ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/greenMatya.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $greenMatya -> name  ?></h5>
                    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $greenMatya -> id]
		    )  ?>
                </div>
	    </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $ispanskayaKlubnika -> price ?><br/><?= ' тг'   ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/ispanskayaKlubnika.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $ispanskayaKlubnika -> name  ?></h5>
                    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $ispanskayaKlubnika -> id]
		    )  ?>
                </div>
	    </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $kivi -> price ?><br/><?= ' тг'   ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/logo_kivi.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $kivi -> name  ?></h5>
                    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $kivi -> id]
		    )  ?>
                </div>
	    </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
                    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $limon -> price ?><br/><?= ' тг'   ?></h4>
                    </div>
                    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/logo_limon.jpg" style="object-fit: cover;">
                    </div>
                    <h5 class="font-weight-bold mb-4"><?= $limon -> name  ?></h5>
                    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $limon -> id]
		    )  ?>
                </div>
	    </div>
	    <div class="col-lg-3 col-md-6 mb-4 pb-2">
                <div class="product-item d-flex flex-column align-items-center text-center bg-light rounded py-5 px-3">
		    <div class="bg-primary mt-n5 py-3" style="width: 80px;">
                        <h4 class="font-weight-bold text-white mb-0"><?= $marokanskiyApelsin -> price ?><br/><?= ' тг'   ?></h4>
		    </div>
		    <div class="position-relative bg-primary rounded-circle mt-n3 mb-4 p-3" style="width: 150px; height: 150px;">
                        <img class="rounded-circle w-100 h-100" src="/user_img/logo_apels.jpg" style="object-fit: cover;">
		    </div>
		    <h5 class="font-weight-bold mb-4"><?= $marokanskiyApelsin -> name  ?></h5>
		    <?= Html::a(
			Html::tag('h4', 'Выбрать',
				  ['class' => 'btn btn-sm btn-secondary' ]),
			['ice/choice', 'id' => $marokanskiyApelsin -> id]
		    )  ?>
                </div>
	    </div>
	    <div class="col-12 text-center">
                <a href="" class="btn btn-primary py-3 px-5">В Начало</a>
	    </div>
        </div>
    </div>
</div>
<!-- Products End -->



