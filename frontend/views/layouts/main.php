<?php
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\BaseAsset;
use common\widgets\Alert;
use backend\models\Telephone;
AppAsset::register($this);
BaseAsset::register($this);

$address = Telephone::findOne(1);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
<meta charset="<?= Yii::$app->charset ?>">
<?php $this->registerCsrfMetaTags() ?>
<title><?= Html::encode($this->title) ?></title>
<?php $this->head() ?>
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="Free HTML Templates" name="keywords">
<meta content="Free HTML Templates" name="description">

<!-- Favicon -->


<!-- Google Web Fonts -->
<link rel="preconnect" href="https://fonts.gstatic.com">


<!-- Font Awesome -->


<!-- Libraries Stylesheet -->



<!-- Customized Bootstrap Stylesheet -->

</head>

<body>
    <?php $this->beginBody() ?>
    <?php  
    NavBar::begin([
	'brandLabel' =>  Html::img('@web/user_img/logo.jpg'),
	'brandUrl' => Yii::$app->homeUrl,
	'options' => [
            'class' => '"navbar navbar-expand-lg bg-white navbar-light shadow p-lg-0"',
	],
    ]);
    
    $menuItems = [
	['label' => 'Купить', 'url' => ['/ice/store']],
	['label' => 'О нас', 'url' => ['/site/index']],
	['label' => 'Контакты', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
	$menuItems[] = ['label' => 'Регистрация', 'url' => ['/site/signup']];
	$menuItems[] = ['label' => 'Войти', 'url' => ['/site/login']];
    } else {
	$menuItems[] = '<li>'
                     . Html::beginForm(['/site/logout'], 'post')
                     . Html::submitButton(
			 'Выйти (' . Yii::$app->user->identity->username . ')',
			 ['class' => 'nav-link']
                     )
                     . Html::endForm()
                     . '</li>';
    }
    echo Nav::widget([
	'options' => ['class' => 'navbar-nav ml-auto py-0'],
	'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <a href="" class="navbar-brand d-block d-lg-none">
	<h1 class="m-0 display-4 text-primary"><span class="text-secondary">i</span>lmiojelato</h1>
    </a>

    <!-- Topbar Start -->
    <div class="container-fluid bg-primary py-3 d-none d-md-block">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center text-lg-left mb-2 mb-lg-0">
                    <div class="d-inline-flex align-items-center">
                        <a class="text-white pr-3" href="">FAQs</a>
                        <span class="text-white">|</span>
                        <a class="text-white px-3" href="">Help</a>
                        <span class="text-white">|</span>
                        <a class="text-white pl-3" href="">Support</a>
                    </div>
                </div>
                <div class="col-md-6 text-center text-lg-right">
                    <div class="d-inline-flex align-items-center">
                        <a class="text-white px-3" href="">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a class="text-white px-3" href="">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a class="text-white px-3" href="">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                        <a class="text-white px-3" href="">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a class="text-white pl-3" href="">
                            <i class="fab fa-youtube"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>


    <!-- Footer Start -->
    <div class="container-fluid footer bg-light py-5" style="margin-top: 90px;">
        <div class="container text-center py-5">
            <div class="row">
                <div class="col-12 mb-4">
                    <a href="index.php" class="navbar-brand m-0">
                        <h1 class="m-0 mt-n2 display-4 text-primary"><span class="text-secondary">i</span>lmiojelato</h1>
                    </a>
                </div>
                <div class="col-12 mb-4">
                    <a class="btn btn-outline-secondary btn-social mr-2" href="#"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-outline-secondary btn-social mr-2" href="#"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-outline-secondary btn-social mr-2" href="#"><i class="fab fa-linkedin-in"></i></a>
                    <a class="btn btn-outline-secondary btn-social" href="#"><i class="fab fa-instagram"></i></a>
                </div>
                <div class="col-12 mt-2 mb-4">
                    <div class="row">
                        <div class="col-sm-6 text-center text-sm-right border-right mb-3 mb-sm-0">
                            <h5 class="font-weight-bold mb-2">Контакты:</h5>
                            <p class="mb-2"></p>
                            <p class="mb-0">+7<?= $address -> telephone  ?></p>
                        </div>
                        <div class="col-sm-6 text-center text-sm-left">
                            <h5 class="font-weight-bold mb-2">Часы работы</h5>
                            <p class="mb-2">Пн – Вс, 8 утра – 9 вечера</p>
                            <p class="mb-0"></p>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <p class="m-0">&copy;<?= Html::encode(Yii::$app -> name)  ?><?= date('Y')  ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->


    <!-- Back to Top -->
    <a href="#" class="btn btn-secondary px-2 back-to-top"><i class="fa fa-angle-double-up"></i></a>


    <!-- JavaScript Libraries -->

    <!-- Contact Javascript File -->

    <!-- Template Javascript -->
<?php $this->endBody() ?>


</html>
<?php $this->endPage() ?>
